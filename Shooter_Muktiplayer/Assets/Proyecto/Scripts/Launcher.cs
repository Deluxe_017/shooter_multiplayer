using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using TMPro;
using Photon.Realtime;

public class Launcher : MonoBehaviourPunCallbacks
{
    public static Launcher Instance;

    [SerializeField] private GameObject []screenObjects;

    [SerializeField] TMP_Text infoText;
    [SerializeField] TMP_Text roomText; 
    [SerializeField] TMP_Text errorText;
    [SerializeField] TMP_InputField roomNameInput;
    [SerializeField] GameObject buttonPref;
    [SerializeField] Transform scrollContent;
    [SerializeField] List<Room_Button> roomButonsList = new List<Room_Button>();


    #region Unity Functions
    private void Awake()
    {
        if(Instance == null)
        {
          Instance = this;
        }
        else
        {
            Destroy(this);
        }
    }
    private void Start()
    {
        infoText.text = "Connecting to Network...";
        SetScreenObject(0);
        PhotonNetwork.ConnectUsingSettings();
    }
    public void Back()
    {
        PhotonNetwork.LeaveRoom();
        SetScreenObject(1);
    }
    #endregion

    #region Photon
    public override void OnConnectedToMaster()
    {
        PhotonNetwork.JoinLobby();
        SetScreenObject(1);
    }
    public override void OnJoinedRoom()
    {
        roomText.text = PhotonNetwork.CurrentRoom.Name;
        SetScreenObject(3);
    }
  public override void OnCreateRoomFailed(short returnCode, string message)
    {
        errorText.text = message;
        SetScreenObject(4);
    }
    #endregion

    #region
    public void SetScreenObject(int index)
    {
        for(int i = 0; i < screenObjects.Length; i++)
        {
            screenObjects[i].SetActive(i == index);
        }
    }
    public void CreateRoom()
    {
        if (!string.IsNullOrEmpty(roomNameInput.text))
        {
            RoomOptions options = new RoomOptions();
            options.MaxPlayers = 8;
            PhotonNetwork.CreateRoom(roomNameInput.text);
            infoText.text = "Creating Room...";
            SetScreenObject(0);
        }
    }

    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        for (int i = 0; i < roomList.Count; i++)
        {
            if (roomList[i].RemovedFromList)
            {
                Debug.Log($"Room Name: {roomList[i].Name}");
                for (int j = 0; j < roomButonsList.Count; j++)
                {
                    if (roomList[i].Name == roomButonsList[j].roomInfo.Name)
                    {
                        GameObject go = roomButonsList[j].gameObject;
                        roomButonsList.Remove(roomButonsList[j]);
                        Destroy(go);
                    }
                }
            }
        }
        for (int i = 0; i < roomList.Count; i++)
        {
           if(roomList[i].PlayerCount != roomList[i].MaxPlayers && !roomList[i].RemovedFromList)
           {
                Room_Button newRoomButton = Instantiate(buttonPref, scrollContent).GetComponent<Room_Button>();
                newRoomButton.SetButtonDetails(roomList[i]);
                roomButonsList.Add(newRoomButton);
           }
        }        
    }
    #endregion
    public void JoinRoom(RoomInfo info)
    {
        PhotonNetwork.JoinRoom(info.Name);
        infoText.text = "Joining Room...";
        SetScreenObject(index: 0);
    }
}