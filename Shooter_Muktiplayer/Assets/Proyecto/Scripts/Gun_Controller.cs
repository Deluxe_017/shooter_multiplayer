using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class Gun_Controller : MonoBehaviour
{
    #region Variables
    public Player_Controller playerController;
    public Gun[] guns;
    public Gun actualGun;
    public int indexGun = 0;
    public int maxGuns = 3;
    public GameObject ammoShot;

    float lastShotTime = 0;
    Vector3 currentRotation;
    Vector3 targerRotation;
    public float returnSpeed;
    public float snappines;

    float lastReload;
    bool reloading;

    public float distancia;
    public LayerMask lm;

    bool isChangin;
    float changeTime;
    float lastChangeTime;

    public GameObject prefabBulletHolle;

    float recoilX;
    float recoilY;
    #endregion

    #region Unity Funtions
    private void Update()
    {
        if (Input.GetButtonDown("E") && Physics.Raycast(playerController.cam.transform.position, playerController.cam.transform.forward, out RaycastHit hit, distancia, lm))
        {
            Transform gun = hit.transform;
            if(hit.transform != null)
            {
                Interaccion(gun, indexGun);
            }
        }
        if (actualGun != null)
        {
            if (lastShotTime <= 0 && !reloading)
            {
                if (!actualGun.data.automatic)
                {
                    if (Input.GetButtonDown("Fire1"))
                    {
                        if (actualGun.data.actualAmmo > 0)
                        {
                            Shoot();
                        }
                    }
                }
                else
                {
                    if (Input.GetButton("Fire1"))
                    {
                        if (actualGun.data.actualAmmo > 0)
                        {
                            Shoot();
                        }
                    }
                }
                if (Input.GetButtonDown("Reload") && !reloading)
                {
                    if (actualGun.data.actualAmmo < actualGun.data.maxAmmoCount)
                    {
                        lastReload = 0;
                        reloading = true;
                        AudioSource.PlayClipAtPoint(actualGun.clip_Recarga, gameObject.transform.position);
                    }
                }
            }
        }
        else
        {

        }

        if (lastShotTime >= 0)
        {
            lastShotTime -= Time.deltaTime;
        }
        if (reloading)
        {
            lastReload += Time.deltaTime;
            if (lastReload >= actualGun.data.reloadTime)
            {
                reloading = false;
                Reload();
            }
        }
        targerRotation = Vector3.Lerp(targerRotation, Vector3.zero, returnSpeed * Time.deltaTime);
        currentRotation = Vector3.Slerp(currentRotation, targerRotation, snappines * Time.deltaTime);
        playerController.recoil.localRotation = Quaternion.Euler(currentRotation);

        if (Input.GetButtonDown("Gun1") && !reloading)
        {
            if (indexGun != 0)
            {
                
                indexGun = 0;
                lastChangeTime = 0;
                if (actualGun != null)
                {
                    AudioSource.PlayClipAtPoint(actualGun.clip_Equipar, gameObject.transform.position);
                    actualGun.gameObject.SetActive(false);
                    actualGun = null;
                }
                isChangin = true;
            }
        }
        if (Input.GetButtonDown("Gun2"))
        {
            if (indexGun != 1)
            {
                
                indexGun = 1;
                lastChangeTime = 0;
                if (actualGun != null)
                {
                    AudioSource.PlayClipAtPoint(actualGun.clip_Equipar, gameObject.transform.position);
                    actualGun.gameObject.SetActive(false);
                    actualGun = null;
                }
                isChangin = true;
            }
        }

        if (isChangin)
        {
            lastChangeTime += Time.deltaTime;
            if(lastChangeTime >= changeTime)
            {
                isChangin = false;
                ChangeGun(indexGun);
            }
        }
    }
    #endregion

    #region Custom Funtions
    private void Shoot()
    {
            if (Physics.Raycast(playerController.cam.transform.position, playerController.cam.transform.forward, out RaycastHit hit, actualGun.data.range))
            {
                if (hit.transform != null)
                {
                    Debug.Log($"We are shootinh at: {hit.transform.name}");
                    GameObject destroy = Instantiate(prefabBulletHolle, hit.point + hit.normal * 0.01f, Quaternion.LookRotation(hit.normal, Vector3.up));
                    Destroy(destroy, 10f);
                }
            }
            actualGun.data.actualAmmo--;
            lastShotTime = actualGun.data.fireRate;
            actualGun.sonido_disparo.PlayOneShot(actualGun.clip);
            Addrecoil();
            GameObject go = VisualEffect.Instantiate(ammoShot, actualGun.muzzlePoint.position, actualGun.muzzlePoint.rotation).gameObject;
            go.transform.parent = actualGun.muzzlePoint;
            Destroy(go, 1f);
    }
    void Addrecoil()
    {
        targerRotation -= new Vector3(actualGun.data.recoil.x, Random.Range(-actualGun.data.recoil.y, actualGun.data.recoil.y), 0f);
    }

    void Reload()
    {
        actualGun.data.actualAmmo = actualGun.data.maxAmmoCount;
        
    }

    void ChangeGun(int index)
    {
        if(guns[index] != null)
        {
            actualGun = guns[index];
            actualGun.gameObject.SetActive(true);
        }
    }

    void Interaccion(Transform Arma_Suelo, int index)
    {
        if (actualGun != null)
        {
            Re_Gun(actualGun, indexGun);
        }

        Rigidbody rb = Arma_Suelo.GetComponent<Rigidbody>();
        rb.isKinematic = true;
        Arma_Suelo.transform.parent = playerController.gunPoint;
        actualGun = Arma_Suelo.transform.gameObject.GetComponent<Gun>();
        Arma_Suelo.transform.localPosition = actualGun.data.offset;
        Arma_Suelo.transform.localRotation = Quaternion.identity;
        guns[index] = actualGun;
    }

    void Re_Gun(Gun Arma_Suelo, int index)
    {
        Rigidbody rb = Arma_Suelo.GetComponent<Rigidbody>();
        actualGun.transform.parent = null;
        guns[index] = null;
        actualGun = null;
        rb.isKinematic = false;
        rb.AddForce(Arma_Suelo.transform.up * 2, ForceMode.Impulse);
        rb.AddTorque(Arma_Suelo.transform.right * 2, ForceMode.Impulse);
    }
    #endregion
}