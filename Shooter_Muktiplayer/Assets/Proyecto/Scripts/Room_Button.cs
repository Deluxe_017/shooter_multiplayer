using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Realtime;
using TMPro;

public class Room_Button : MonoBehaviour
{
    #region Variables

    public RoomInfo roomInfo;
    [SerializeField] private TMP_Text buttonText;

    #endregion

    public void SetButtonDetails(RoomInfo inputInfo)
    {
        roomInfo = inputInfo;
        buttonText.text = roomInfo.Name;
    }
    public void JoinRoom()
    {
        Launcher.Instance.JoinRoom(roomInfo);
    }
}
