using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{
    #region Variables
    public Gun_Data data;
    public Transform muzzlePoint;

    public AudioClip clip;
    public AudioSource sonido_disparo;

    public AudioClip clip_Recarga;
    public AudioSource sonido_Recarga;

    public AudioClip clip_Equipar;
    public AudioSource sonido_Equipar;
    #endregion

    #region Unity Funtions
    private void Awake()
    {
        data.actualAmmo = data.maxAmmoCount;
    }
    #endregion
}
